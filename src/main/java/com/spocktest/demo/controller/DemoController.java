package com.spocktest.demo.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {


    @GetMapping(value = "/get-hello")
    public String getHello() {
        return "Hello";
    }

    @GetMapping(value = "/greet-user")
    public String greetUser() {
        return "Hello dear user!";
    }


}
