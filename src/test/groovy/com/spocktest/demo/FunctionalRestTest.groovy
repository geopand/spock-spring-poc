package com.spocktest.demo

import org.springframework.boot.test.context.SpringBootTest
import org.springframework.web.client.RestTemplate
import spock.lang.Specification



/**
 *  This test shows how to load the whole application to perform functional test
 **/
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
// SpringBootTest loads the whole application and reads the port from the application properties
class FunctionalRestTest extends Specification {

    RestTemplate restTemplate = new RestTemplate()

    def "The get-hello endpoint returns 'Hello'"() {

        expect: "The response is 'Hello'"
        String response = restTemplate.getForObject('http://127.0.0.1:8080/get-hello', String.class)
        assert 'Hello' == response


    }


}
