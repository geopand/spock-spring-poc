package com.spocktest.demo

import com.spocktest.demo.controller.DemoController
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest
class SmokeTest extends Specification{

    @Autowired
    DemoController demoController

    def "The application context starts with no issues"(){

        expect : "The application context to start"
        demoController

    }
}
