package com.spocktest.demo

import com.spocktest.demo.controller.DemoController
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@WebMvcTest(DemoController)
class ControllerLayerTest extends Specification {


    @Autowired
    MockMvc mockMvc

    def "The greet-user endpoint returns 'Hello dear user!'"() {

        given: "After calling the service"
        def response = mockMvc.perform(get("/greet-user"))

        expect: "Controller returns Http 200,and the response is  'Hello dear user!'"

        response
                .andExpect(status().isOk())
                .andExpect(content().string("Hello dear user!"))

    }



}
